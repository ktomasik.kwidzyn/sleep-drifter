# Sleep Drifter

Sleep Drifter is a simple library I wrote to eliminate nasty flashes of white light when I was changing subpages on my dark backgrounded site. Maybe I will expand on it one day, maybe naught.

### Usage
All you need, after loading the script, is:
```js
n = new SleepDrifter(root);
n.init(your_initial_subpage_url);
```

You might also want to register some links outside the root element. You can do this with `n.registerLinks(element)` method.


### Limitations

Right now, it will not load script and css files, only plain html. Also, all external links shouls start with `http`, otherwise the engine will treat them as subpage links.

### TODOS:
- [X] Upload this onto gitlab.
- [ ] Fix limitations mentioned above(Make a proper distinction between subpage links and external links).
- [ ] Expand the lib with a nice, optional, loading tab.
- [ ] Expand the lib with ability to load js scripts in some way.
- [ ] Expand the lib with ability to load and unload css styles.
