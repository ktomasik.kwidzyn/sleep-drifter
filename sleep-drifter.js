class SleepDrifter extends Object {
  constructor(root, prefix="") {
    super();
    this.root = root;
    this.currentContent = "";
    this.loading = false;
    this.prefix = prefix;
  }

  setRoot(newRoot) {
    this.root = newRoot;
  }

  display() {
    this.root.style.filter = this.loading
      ? "blur(4px)"
      : "";
    if(this.root.innerHTML == this.currentContent.innerHTML){return;}
    this.root.innerHTML = this.currentContent.innerHTML;
  }

  fetchSubpage(url, push=true) {
    this.loading = true;
    this.display();
    if(push){window.history.pushState(url,url,url);}
    fetch(this.prefix+url).then(resp => {
      if(!resp.ok) {
        console.log(`Smth went wrong (aside from having no normal error messages). Response status: ${resp.status}`);
        return
      }
      resp.text().then(body => {
        let tempDiv = document.createElement("div");
        tempDiv.innerHTML = body;
        Array.from(tempDiv.getElementsByTagName("a")).forEach(element => {
          console.log("test0");
          if(!element.getAttribute('href').startsWith("http")){
            element.addEventListener('click', this.onLinkClick);
            console.log("test");
          }
        })
        this.currentContent = tempDiv;
        this.loading = false;
        this.display();
      });
    });
  }

  init(initUrl="") {
    if(initUrl){
      this.fetchSubpage(initUrl, false);
    }
    window.addEventListener('popstate', (evt) => {
      if(window.location.pathname){
        this.fetchSubpage(window.location.pathname, false);
      }
    });
  }

  registerLinks(root) {
    Array.from(root.getElementsByTagName("a")).forEach(element => {
      console.log(element);
      if(!element.getAttribute('href').startsWith("http")){
        element.addEventListener('click', (evt) => {evt.preventDefault();this.fetchSubpage(element.getAttribute('href'))});
        console.log(element.getAttribute('href'));
      }
    })
  }

}
